terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.70.0"
    }
  }
}




provider "aws" {
    region = "us-east-1"
     
}
#creo el s3
resource "aws_s3_bucket" "b1"{
    bucket = "mi-tf-test-mavbucket"
    acl = "public-read"
    tags = {
        Name = "mibucket"
        Enviromente = "Dev"
    } 
}

# actualizao el objeto   
resource "aws_s3_bucket_object" "obj1"{
    for_each = fileset("miarchivo/", "*")     
    bucket = aws_s3_bucket.b1.id
    key = each.value
    source = "miarchivo/${each.value}"
    etag = filemd5("miarchivo/${each.value}")
   }